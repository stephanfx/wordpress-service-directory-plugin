<p>
	<label for="TelephoneNr">Telephone Nr</label>
	<input type="text" name="telephone_nr" id="telephone_nr" value="<?php echo $telephone_nr; ?>">
</p>
<p>
	<label for="FaxNr">Fax Nr</label>
	<input type="text" name="fax_nr" id="fax_nr" value="<?php echo $fax_nr; ?>">
</p>
<p>
	<label for="EmailAddress">Email Address</label>
	<input type="text" name="email_address" id="email_address" value="<?php echo $email_address; ?>">
</p>
<p>
	<label for="WebsiteAddress">Website Address</label>
	<input type="text" name="website_address" id="website_address" value="<?php echo $website_address; ?>">
</p>
<p>
	<label for="PhysicalAddress">Physical Address</label>
	<input type="text" name="physical_address" id="physical_address" value="<?php echo $physical_address; ?>">
</p>
<p>
	<label for="PostalAddress">Postal Address</label>
	<input type="text" name="postal_address" id="postal_address" value="<?php echo $postal_address; ?>">
</p>
<p>
	<label for="latitude">Latitude:</label>
	<input type="text" name="latitude" id="latitude" value="<?php echo $latitude; ?>">
</p>
<p>
	<label for="longitude">Longitude:</label>
	<input type="text" name="longitude" id="longitude" value="<?php echo $longitude; ?>">
</p>