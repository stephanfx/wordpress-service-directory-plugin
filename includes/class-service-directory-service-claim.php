<?php

/**
* JSON Endpoint to send mail to Vetlink for the enable ment of the service directory changes.
*/
class Service_Directory_Claim
{

	function register_routes($routes)
	{
		$routes['/services/claim'] = array(
			array( array( $this, 'claim_service' ), WP_JSON_Server::CREATABLE | WP_JSON_Server::ACCEPT_JSON ),
		);
		return $routes;
	}

	public function claim_service( $data )
	{
		// get only relevant data
		$listing = array(
			'ID' => $data['ID'],
			'title' => $data['title'],
			'tel_nr' => isset($data['tel_nr']) ? $data['tel_nr'] : null,
			'fax_nr' => isset($data['fax_nr']) ? $data['fax_nr'] : null,
			'email_address' => isset($data['email_address']) ? $data['email_address'] : null,
			'physical_address' => isset($data['physical_address']) ? $data['physical_address'] : null,
			'postal_address' => isset($data['postal_address']) ? $data['postal_address'] : null,
			'website_address' => isset($data['website_address']) ? $data['website_address'] : null,
			'content' => isset($data['content']) ? $data['content'] : null,
		);
		// save the contact to a csv as the mails dont seem to work
		$file = fopen(plugin_dir_path(dirname(__FILE__)) . 'claims.csv', 'a');
		fputcsv($file, $listing);
		fclose($file);

		$body =  'There was a new claim on ' . $listing['title'] . '' . PHP_EOL ;
		foreach ($listing as $key => $value) {
			$body .= $key . " : " . $value . PHP_EOL;
		}
		$body .= "";

		$headers = 'Bcc: stephan.grobler+vettestmails@gmail.com';
		$result = wp_mail( 'admin@vetlink.co.za', 'Claim on listing: ' . $listing['title'], $body, $headers);
		if (!$result){
			return new WP_Error( 'json_error_sending_mail', __( 'There was an error sending the mail.' ), array( 'status' => 500 ) );
		}
		return array('result' => 'success');
	}


}
