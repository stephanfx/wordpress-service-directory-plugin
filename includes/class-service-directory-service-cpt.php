<?php

/**
* Service Custom Post Type
*/
class Service_Directory_Service_CustomPostType
{

	function __construct($post_id = null)
	{
		if (!empty($post_id)){
			$this->getPost($post_id);
		}
	}

	public function getPost($post_id)
	{
		# code...
	}

	public function addMetaBoxes()
	{
		add_meta_box(
			'service_meta',
			'Additional Service Information',
			array('Service_Directory_Service_CustomPostType', 'getMetaBox'),
			'service',
			'normal'
		);
	}

	public function getMetaBox($post)
	{
		$telephone_nr = get_post_meta($post->ID, '_service_telephone_nr', 1);
		$fax_nr = get_post_meta($post->ID, '_service_fax_nr', 1);
		$email_address = get_post_meta($post->ID, '_service_email_address', 1);
		$website_address = get_post_meta($post->ID, '_service_website_address', 1);
		$physical_address = get_post_meta($post->ID, '_service_physical_address', 1);
		$postal_address = get_post_meta($post->ID, '_service_postal_address', 1);
		$latitude = get_post_meta($post->ID, '_service_latitude', 1);
		$longitude = get_post_meta($post->ID, '_service_longitude', 1);

		include_once plugin_dir_path(__FILE__) . '../admin/partials/service-directory-service-meta-box.php';
	}

	public function apiInit($server)
	{
		global $service_directory_service_api;

		$service_directory_service_api = new Service_Directory_Service_API($server);
		$service_directory_service_api->register_filters();

	}

	public function savePost($post_id)
	{
		if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
			return $post_id;
		}

		if (isset($_POST['post_type']) && 'service' == $_POST['post_type']){
			if (!current_user_can('edit_page', $post_id)){
				return $post_id;
			}
			update_post_meta($post_id, '_service_telephone_nr', sanitize_text_field($_POST['telephone_nr']));
			update_post_meta($post_id, '_service_fax_nr', sanitize_text_field($_POST['fax_nr']));
			update_post_meta($post_id, '_service_email_address', sanitize_email($_POST['email_address']));
			update_post_meta($post_id, '_service_website_address', esc_url($_POST['website_address']));
			update_post_meta($post_id, '_service_physical_address', sanitize_text_field($_POST['physical_address']));
			update_post_meta($post_id, '_service_postal_address', sanitize_text_field($_POST['postal_address']));
			update_post_meta($post_id, '_service_latitude', sanitize_text_field($_POST['latitude']));
			update_post_meta($post_id, '_service_longitude', sanitize_text_field($_POST['longitude']));
		} else {
			if (!current_user_can('edit_post', $post_id)){
				return $post_id;
			}
		}
	}

	public function init()
	{
		register_post_type('service', array(
			'labels' => array(
				'name' => __('Services'),
				'singular_name' => __('Service'),
			),
			'public' => true,
			'has_archive' => true,
			'supports' => array(
				'title',
				'editor',
				'excerpt',
				'thumbnail'
			),
			)
		);

		register_taxonomy(
			'section',
			'service',
			array(
				'label' => __('Sections'),
				'labels' => array(
					'name' => 'Sections',
					'singular_name' => 'Section',
					'search_items' => 'Search Sections',
					'edit_item' => 'Edit Section',
					'add_new_item' => 'Add new Section',
				),
				'rewrite' => array('slug' => 'section'),
				'hierarchical' => true,
			)
		);
	}
}