<?php

/**
* Import Class
*/
class Service_Directory_Import
{

	function import($file = null)
	{
		$file = dirname(__FILE__) . '/../import/services2.csv';
		if (is_file($file)){
			$fh = fopen($file, 'r');
			$count = 0;
			// step through the records
			while (($data = fgetcsv($fh, 10000, ", ")) !== false) {
				$num = count($data);
				// check if the section exists and create it if not
				$term = get_terms('section', array('search' => $data[0]));
				if (!$term){
					wp_insert_term($data[0], 'section');
				}

				//set initial data to be used
				$post_data = array(
					'post_title' => sanitize_text_field( $data[1] ),
					'post_author' => 1,
					'post_type' => 'service',
					'comment_status' => 'Closed',
					'post_status' => 'publish',
					);

				// check if post exists or not
				global $wpdb;
				$sqlQuery = "SELECT ID FROM $wpdb->posts WHERE post_type = 'service' AND post_title = '{$data[1]}'";
				$services = $wpdb->get_col($sqlQuery);
				if (count($services)){
					$post_id = $services[0];
					$post_data['ID'] = $post_id;
					wp_update_post($post_data);
				} else {
					$post_id = wp_insert_post(
						$post_data
					);
				}

				update_post_meta($post_id, '_service_telephone_nr', sanitize_text_field($data[2]));
				// update_post_meta($post_id, '_service_fax_nr', sanitize_text_field($_POST['fax_nr']));
				// update_post_meta($post_id, '_service_email_address', sanitize_email($_POST['email_address']));
				// update_post_meta($post_id, '_service_website_address', esc_url($_POST['website_address']));
				// update_post_meta($post_id, '_service_physical_address', sanitize_text_field($_POST['physical_address']));
				// update_post_meta($post_id, '_service_postal_address', sanitize_text_field($_POST['postal_address']));

				// set service to section
				wp_set_object_terms($post_id, $data[0], 'section');

				$count++;
			}
			fclose($fh);
			echo $count . ' record(s) inserted';
		}
	}
}